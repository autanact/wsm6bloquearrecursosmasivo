
/**
 * WsM6BloquearRecursosMasivosServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WsM6BloquearRecursosMasivosServiceSkeleton java skeleton for the axisService
     */
    public class WsM6BloquearRecursosMasivosServiceSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WsM6BloquearRecursosMasivosServiceLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param fechaSolicitud
                                     * @param direccion
                                     * @param ofertaEconomica
                                     * @param detalleRespuesta
                                     * @param tecnologias
                                     * @param notas
                                     * @param atributos
         */
        

                 public co.net.une.www.ncainvm6.M6BloquearRecursosMasivosType M6BloquearRecursosMasivos
                  (
                  co.net.une.www.ncainvm6.UTCDate fechaSolicitud,co.net.une.www.ncainvm6.Direcciontype direccion,java.lang.String ofertaEconomica,co.net.une.www.ncainvm6.Detallerespuestatype detalleRespuesta,co.net.une.www.ncainvm6.Listatecnologiastype tecnologias,co.net.une.www.ncainvm6.Listanotastype notas,co.net.une.www.ncainvm6.Listaatributostype atributos
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("fechaSolicitud",fechaSolicitud);params.put("direccion",direccion);params.put("ofertaEconomica",ofertaEconomica);params.put("detalleRespuesta",detalleRespuesta);params.put("tecnologias",tecnologias);params.put("notas",notas);params.put("atributos",atributos);
		try{
		
			return (co.net.une.www.ncainvm6.M6BloquearRecursosMasivosType)
			this.makeStructuredRequest(serviceName, "M6BloquearRecursosMasivos", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    